-module(rtbdemo_request).
-export([new/1, rec/1]).

-include("rtbdemo_request.hrl").

-compile({parse_transform, exprecs}).
-export_records([bid_request]).

new(<<"bid_request">>) ->
  '#new-bid_request'();

new(_RecName) ->
  undefined.

rec(#bid_request{}) -> true;
rec(_) -> false.
