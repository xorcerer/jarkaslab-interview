%% @author Mochi Media <dev@mochimedia.com>
%% @copyright rtbdemo Mochi Media <dev@mochimedia.com>

%% @doc Callbacks for the rtbdemo application.

-module(rtbdemo_app).
-author("Mochi Media <dev@mochimedia.com>").

-behaviour(application).
-export([start/2,stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for rtbdemo.
start(_Type, _StartArgs) ->
    rtbdemo_deps:ensure(),
    rtbdemo_sup:start_link().

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for rtbdemo.
stop(_State) ->
    ok.
