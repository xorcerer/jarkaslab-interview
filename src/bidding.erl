-module(bidding).
-export([save_bid_request/1]).

-include("rtbdemo_request.hrl").


to_mongojson([]) -> [];

to_mongojson(MochiJson) ->
  [{K, V} | Tail] = MochiJson,
  [binary_to_atom(K, utf8), V | to_mongojson(Tail)].

mochijson_to_mongojson(MochiJson) -> list_to_tuple(to_mongojson(MochiJson)).

save_bid_request(JsonStr) ->
  ErlJsonInput = mochijson2:decode(JsonStr),
  Record = json_rec:to_rec(ErlJsonInput, rtbdemo_request, rtbdemo_request:new(<<"bid_request">>)),

  {struct, JsonToSave} = json_rec:to_json(Record, rtbdemo_request),

  %% TODO: Move to outer scope.
  {ok, Connection} = mongo:connect("127.0.0.1", 27017, <<"rtb">>),
  mongo:insert(Connection, <<"biddings">>, [mochijson_to_mongojson(JsonToSave)]),

  ok.