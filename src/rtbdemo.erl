%% @author Mochi Media <dev@mochimedia.com>
%% @copyright 2010 Mochi Media <dev@mochimedia.com>

%% @doc rtbdemo.

-module(rtbdemo).
-author("Mochi Media <dev@mochimedia.com>").
-export([start/0, stop/0]).

ensure_started(App) ->
  case application:start(App) of
    ok ->
      ok;
    {error, {already_started, App}} ->
      ok
  end.


%% @spec start() -> ok
%% @doc Start the rtbdemo server.
start() ->
  rtbdemo_deps:ensure(),
  ensure_started(crypto),
  ensure_started(bson),
  ensure_started(mongodb),

  application:start(rtbdemo).


%% @spec stop() -> ok
%% @doc Stop the rtbdemo server.
stop() ->
  application:stop(rtbdemo).
