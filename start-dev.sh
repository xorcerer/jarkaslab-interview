#!/bin/sh
exec erl \
    -pa ebin deps/*/ebin \
    -boot start_sasl \
    -sname rtbdemo_dev \
    -s rtbdemo \
    -s reloader
